#include "sscan.h"

int main(int argc, char * argv[]) { 
  char * dev;
  pcap_t * handle;
  bpf_u_int32 net, mask;

  dev = get_dev();
  handle = get_handle(dev);
  set_netmask(dev, &net, &mask);
  set_filter(handle,net,argv[1]);
  pcap_loop(handle, 0, process_packet, NULL);

  pcap_close(handle); 
  return(0);
}

void process_packet(u_char * args, const struct pcap_pkthdr * header,
		    const u_char * packet) {
  disassemble_packet(packet, header);
}

void disassemble_packet(const u_char * packet, const struct pcap_pkthdr * hdr) {
  const time_t * tv_sec = &(hdr->ts.tv_sec);
  printf("\n");
  disass_eth(packet);
  printf("Length: %d | %s", hdr->len, ctime((const time_t*)tv_sec));
}

void disass_eth(const u_char * packet) {
  int i; struct ether_header * eth = (struct ether_header *) packet;
  int types[4] = {ETHERTYPE_IP,ETHERTYPE_IPV6,ETHERTYPE_ARP,ETHERTYPE_AT};
  char * strs[4] = {"(IP) | ","(IPV6) | ","(ARP) | ","(AT) | "};
  for(i=0;i<4;++i) 
    if(ntohs(eth->ether_type)==types[i]){printf("%s",strs[i]);}
  printf("%s -> ",ether_ntoa((const struct ether_addr *)&eth->ether_shost));
  printf("%s\n",ether_ntoa((const struct ether_addr *)&eth->ether_dhost));
}

char * get_dev() {
  char * dev = pcap_lookupdev(errbuf);
  mem_error_check(dev);
  printf("Device: %s\n", dev);
  return dev;
}

pcap_t * get_handle(char * dev) {
  pcap_t * handle = pcap_open_live(dev, BUFSIZ, 1, -1, errbuf);
  mem_error_check(handle);
  return handle;
}

void set_netmask(char * dev, bpf_u_int32 * net, bpf_u_int32 * mask) {
  val_error_check(pcap_lookupnet(dev, net, mask, errbuf));
}

void set_filter(pcap_t * handle, bpf_u_int32 net, char * filter_exp) {
  struct bpf_program fp;
  val_error_check(pcap_compile(handle, &fp, filter_exp, 0, net));
  val_error_check(pcap_setfilter(handle, &fp));
}
void mem_error_check(void * obj) {
  if (obj == NULL) 
    error_and_exit();
}
void val_error_check(int val) {
  if (val == -1) 
    error_and_exit();
}
void error_and_exit() {
  fprintf(stderr, "Something happened: %s\n", errbuf);
  exit(EXIT_FAILURE);
}
