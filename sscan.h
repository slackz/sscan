#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pcap.h>
#include <netinet/ether.h>
#include <netinet/ip.h>

void process_packet(u_char *args, const struct pcap_pkthdr *header, 
		    const u_char * packet);
void disassemble_packet(const u_char * packet, const struct pcap_pkthdr * hdr);
void disass_eth(const u_char * packet);
char * get_dev();
pcap_t * get_handle(char * dev);
void set_netmask(char * dev, bpf_u_int32 * net, bpf_u_int32 * mask);
void set_filter(pcap_t * handle, bpf_u_int32 net, char * filter_exp);
void mem_error_check(void * obj);
void val_error_check(int val);
void error_and_exit();


char errbuf[PCAP_ERRBUF_SIZE];
